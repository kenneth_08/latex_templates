# Latex Templates
Templates for different document types are rolled-out in different branches.

## Branches:
* thesis
* short-document
* presentation

## Usage
1. Clone the repository with the folder name you want.
2. Checkout the branch for your specific type of document.
3. Remove the .git folder
4. Optional, initialize the folder for a other repository (when working in git)
5. Modify the content of the *.tex files
6. Compile (on linux) by running 'make' (executing the Makefile)

